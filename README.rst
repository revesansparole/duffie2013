========================
duffie2013
========================

.. {# pkglts, doc

.. image:: https://b326.gitlab.io/duffie2013/_images/badge_pkging_pip.svg
    :alt: PyPI version
    :target: https://pypi.org/project/duffie2013/0.2.0/

.. image:: https://b326.gitlab.io/duffie2013/_images/badge_pkging_conda.svg
    :alt: Conda version
    :target: https://anaconda.org/revesansparole/duffie2013

.. image:: https://b326.gitlab.io/duffie2013/_images/badge_doc.svg
    :alt: Documentation status
    :target: https://b326.gitlab.io/duffie2013/

.. image:: https://badge.fury.io/py/duffie2013.svg
    :alt: PyPI version
    :target: https://badge.fury.io/py/duffie2013

.. #}
.. {# pkglts, glabpkg_dev, after doc

main: |main_build|_ |main_coverage|_

.. |main_build| image:: https://gitlab.com/b326/duffie2013/badges/main/pipeline.svg
.. _main_build: https://gitlab.com/b326/duffie2013/commits/main

.. |main_coverage| image:: https://gitlab.com/b326/duffie2013/badges/main/coverage.svg
.. _main_coverage: https://gitlab.com/b326/duffie2013/commits/main
.. #}

Implementation of formalisms from Duffie 2013 on solar engineering

