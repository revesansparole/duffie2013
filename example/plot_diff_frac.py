"""
Diffuse fraction of radiation
=============================

Plot formalism for estimating diffuse fraction of radiation and
compare it to fig2.10.3
"""
from datetime import datetime, timedelta
from math import radians

import matplotlib.dates as mdates
import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
from duffie2013 import clear_sky, extraterrestrial
from duffie2013.diffuse import diff_frac

day = datetime(2020, 7, 1)
latitude = 45.
longitude = 0.

# compute evolution of formalism
rg_ext, _ = extraterrestrial.rg_ground(day + timedelta(hours=12), radians(latitude), radians(longitude))
rgs = np.linspace(0, 1, 100) * rg_ext

diff_fracs = [diff_frac(rg, rg_ext) for rg in rgs]
kts = rgs / rg_ext

# compute evolution throughout typical day
records = []
for h in np.linspace(0, 24, 100):
    date = day + timedelta(hours=h)
    rg_ext, _ = extraterrestrial.rg_ground(date, radians(latitude), radians(longitude))
    rg = sum(clear_sky.rg_ground(date, radians(latitude), radians(longitude)))
    diff_ratio = diff_frac(rg, rg_ext)

    records.append(dict(
        date=date,
        rg_ext=rg_ext,
        rg=rg,
        diff_frac=diff_ratio,
        rg_diff=rg * diff_ratio
    ))

df = pd.DataFrame(records).set_index('date')

# plot result
fig, axes = plt.subplots(2, 2, sharex='col', figsize=(12, 8), squeeze=False, gridspec_kw={'height_ratios': [3, 1]})

ax = axes[0, 0]
ax.plot(kts, diff_fracs)
ax.set_xlabel("kt [-]")
ax.set_ylabel("diffuse fraction [-]")

ax = axes[0, 1]
ax.plot(df.index, df['rg'], label="rg")
ax.plot(df.index, df['rg_ext'], '--', label="rg_ext")
ax.fill_between(df.index, 0, df['rg_diff'], label="rg_diff")

ax.legend(loc='upper left')
ax.set_xlabel("time of day")
ax.set_ylabel("rg [W.m-2]")

ax = axes[1, 1]
ax.plot(df.index, df['rg'] / df['rg_ext'], label="rg")

ax.xaxis.set_major_formatter(mdates.DateFormatter("%H:%M"))

fig.tight_layout()
plt.show()
