"""
Rg course throught the day on flat ground
=========================================

Plot rg_ground on specific times and locations
"""
from datetime import datetime, timedelta
from math import radians

import matplotlib.dates as mdates
import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
from duffie2013 import clear_sky, extraterrestrial

dates = (datetime(2020, 1, 1), datetime(2020, 7, 1))
latitudes = (15, 45)
longitude = 0.

dmy_day = dates[0]  # dummy day used to align time of day only

records = []
for latitude in latitudes:
    for day in dates:
        for h in np.linspace(0, 24, 100):
            date = day + timedelta(hours=h)
            records.append(dict(
                day=day,
                date=date,
                time=dmy_day + timedelta(hours=h),
                latitude=latitude,
                rg_ext=sum(extraterrestrial.rg_ground(date, radians(latitude), radians(longitude))),
                rg_clear=sum(clear_sky.rg_ground(date, radians(latitude), radians(longitude))),
            ))

df = pd.DataFrame(records)

# plot result
fig, axes = plt.subplots(len(latitudes), 1, sharex='all', figsize=(12, 6), squeeze=False)

for i, (latitude, df_lat) in enumerate(df.groupby('latitude')):
    ax = axes[i, 0]
    for day, sdf in df_lat.groupby('day'):
        crv, = ax.plot(sdf['time'], sdf['rg_clear'], label=f"{day.date().isoformat()}")
        ax.plot(sdf['time'], sdf['rg_ext'], '--', color=crv.get_color())

    ax.legend(loc='upper left', title=f"lat: {latitude:.0f}°N")
    ax.set_xlabel("time of day")
    ax.set_ylabel("rg [W.m-2]")

axes[-1, 0].xaxis.set_major_formatter(mdates.DateFormatter("%H:%M"))

fig.tight_layout()
plt.show()
